import random

#pop() pops value off the end of the list

suits = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
ranks = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace')
values = {'Two':2, 'Three':3, 'Four':4, 'Five':5, 'Six':6, 'Seven':7, 'Eight':8, 'Nine':9, 'Ten':10, 'Jack':10, 'Queen':10, 'King':10, 'Ace':11}

playing = True



class Card():

	def __init__(self, suit, rank):

		self.suit = suit
		self.rank = rank
		self.value = values[rank]

	def __str__(self):
		return self.rank + ' of ' + self.suit



class Deck():
    
	def __init__(self):
		self.full_deck = []  # start with an empty list
		for suit in suits:
			for rank in ranks:
				new_card = Card(suit, rank)
				self.full_deck.append(new_card)
    
	def __str__(self):
		deck_comp = ''
		for card in self.full_deck:
			deck_comp += '\n' + str(card) 
		return 'The deck has:' + deck_comp

	def shuffle(self):
		random.shuffle(self.full_deck)
        
	def deal(self):
		return self.full_deck.pop()


class Hand():

	def __init__(self):
		self.cards_in_hand = [] 	#start with empty hand
		self.value = 0 				#start with empty value
		self.aces = 0 				#add attribute to keep track of aces

	def add_card(self, card):
		#track aces
		if card.rank == 'Ace':
			self.aces += 1

		#add card to deck
		self.cards_in_hand.append(card)

		#add value to hand
		self.value += card.value

	def adjust_for_ace(self):
		#if hand is greater than 21 and aces are in hand decremnt by 10 and drop ace count 
		while self.value > 21 and self.aces:
			self.value -= 10
			self.aces -= 1

	def print_cards(self):
		#print out cards in hand
		for cards in self.cards_in_hand:
			print(cards)

class Chips:

	def __init__(self, total=100):
		self.total = total
		self.bet = 0

	def win_bet(self, bet1):
		self.total += bet1

	def lose_bet(self, bet1):
		self.total -= bet1


def take_bet(player_chips):
	#takes a user inputed bet amount that must be a positive integer
	while True:
		try:
			bet_value = int(input('Please make your bet: '))
		except:
			print('Bet number is not valid, must be an integer.')
			continue
		else:
			if bet_value > player_chips:
				print(f'Insufficient chip amount, you only have {player_chips} chips.')
				continue
			elif bet_value <= 0:
				print('Please bet a value that is greater than 0.')
				continue
			else:
				print(f'bet of {bet_value} accepted.\n')
				return bet_value
				

def hit(deck, hand):
	dealt_card = deck.deal()
	hand.add_card(dealt_card)
	hand.adjust_for_ace()


def hit_or_stand(deck,hand):
	global playing #to control gameplay while loop
	while True:
		decision = input('Hit or Stand? (h/s): ')
		if decision != 'H' and decision != 'S' and decision != 'h' and decision != 's':
			print('please input an ''H'' or ''S'' indicating hit or stand')
			continue
		elif decision == 'H' or decision == 'h':
			print('Hit!\n')
			hit(deck, hand)
			break
		elif decision == 'S' or decision == 's':
			print('Stand!\n')
			playing = False
			break
		else:
			break
	return

def show_some(player, dealer):
	#shows 1 dealer card and all players cards + total
	print('Dealers Hand:')
	print(dealer.cards_in_hand[0])
	print('*Hidden Card*')
	print(f'Value: {dealer.cards_in_hand[0].value}\n')
	print('Players Hand:')
	for card in player.cards_in_hand:
		print(card)
	print(f'Value: {player.value}\n')


def show_all(player, dealer):
	#shows all cards and their totals 
	print('Dealers Hand:')
	for card in dealer.cards_in_hand:
		print(card)
	print(f'Value: {dealer.value}\n')

	print('Players Hand:')
	for card in player.cards_in_hand:
		print(card)
	print(f'Value: {player.value}\n')


def player_busts(chips, num_chips):
	print('Player Busts :( ')
	print(f'Lost {num_chips} chips.\n')
	chips.lose_bet(num_chips)

def player_wins(chips, num_chips):
	print('Player Wins! :) ')
	print(f'Gain {num_chips} chips!\n')
	chips.win_bet(num_chips)

def dealer_busts(chips, num_chips):
	print('Dealer Busts! :) ')
	print(f'Gain {num_chips} chips!\n')
	chips.win_bet(num_chips)

def dealer_wins(chips, num_chips):
	print('Dealer Wins :( ')
	print(f'Lost {num_chips} chips.\n')
	chips.lose_bet(num_chips)

def push(num_chips):
	print('Push! No chips gained or lost.')
	print(f'Keep {num_chips} chips.\n')


players_chips = Chips(100)

while True:
	# Print an opening statement
	print('\nNew game started! Beat the dealer to win chips!')

	# Create & shuffle the deck, deal two cards to each player
	new_deck = Deck()
	new_deck.shuffle()
	player_hand = Hand()
	dealer_hand = Hand()
	player_hand.add_card(new_deck.deal())
	dealer_hand.add_card(new_deck.deal())
	player_hand.add_card(new_deck.deal())
	dealer_hand.add_card(new_deck.deal())
	player_busted = False

	# Set up the Player's chips
	print(f'Player has {players_chips.total} chips to bet.')

	# Prompt the Player for their bet
	bet = take_bet(players_chips.total)

	# Show cards (but keep one dealer card hidden)
	show_some(player_hand,dealer_hand)
	playing = True

	while playing:  # recall this variable from our hit_or_stand function
		
		# Prompt for Player to Hit or Stand
		hit_or_stand(new_deck, player_hand)
		
		# Show cards (but keep one dealer card hidden)
		show_some(player_hand,dealer_hand)
		
		# If player's hand exceeds 21, run player_busts() and break out of loop
		if player_hand.value > 21:
			player_busts(players_chips,bet)
			player_busted = True
			break

	# If Player hasn't busted, play Dealer's hand until Dealer reaches 17
	if not player_busted:
		while dealer_hand.value < 17:
			dealer_hand.add_card(new_deck.deal())

		# Show all cards
		show_all(player_hand,dealer_hand)
		# Run different winning scenarios
		if dealer_hand.value > 21:
			dealer_busts(players_chips,bet)
		elif player_hand.value < dealer_hand.value:
			dealer_wins(players_chips,bet)
		elif player_hand.value > dealer_hand.value:
			player_wins(players_chips,bet)
		else:
			push(bet)

	# Inform Player of their chips total 
	print(f'Player now has {players_chips.total} chips!')

	if players_chips.total == 0:
		print('Player is out of chips :(')
		print('Thanks for playing :)')
		break

	# Ask to play again
	while True:
		decision = input('Play Again? (y/n): ')
		if decision != 'Y' and decision != 'y' and decision != 'N' and decision != 'n':
			print('please input an ''Y'' or ''N'' indicating yes or no')
			continue
		else:
			break

	if decision == 'Y' or decision == 'y':
		continue
	else: 
		print('Thanks for playing :)')
		break











		
