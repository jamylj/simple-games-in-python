#Def global variables
board_list = [1,2,3,4,5,6,7,8,9]
MAX_TURNS = 9
current_turn = 0
letter = 'X'
X_win = False
O_win = False

def display_board():
	print('\n')
	print(' ', board_list[0], ' | ', board_list[1], ' | ', board_list[2])
	print(' ---','+','---','+','---')
	print(' ', board_list[3], ' | ', board_list[4], ' | ', board_list[5])
	print(' ---','+','---','+','---')
	print(' ', board_list[6], ' | ', board_list[7], ' | ', board_list[8])
	print('\n')
	return

def make_move(XorO, position):
	board_list[position-1] = XorO
	return

def check_open(position):
	if board_list[position-1] not in range(1,10):
		return False
	else:
		return True

def check_win():
	#8 win cases:
	if board_list[0] == board_list[1] and board_list[1] == board_list[2]:
		return True
	elif board_list[3] == board_list[4] and board_list[4] == board_list[5]:
		return True
	elif board_list[6] == board_list[7] and board_list[7] == board_list[8]:
		return True
	elif board_list[0] == board_list[3] and board_list[3] == board_list[6]:
		return True
	elif board_list[1] == board_list[4] and board_list[4] == board_list[7]:
		return True
	elif board_list[2] == board_list[5] and board_list[5] == board_list[8]:
		return True
	elif board_list[0] == board_list[4] and board_list[4] == board_list[8]:
		return True
	elif board_list[6] == board_list[4] and board_list[4] == board_list[2]:
		return True
	else:
		return False

####    MAIN    ####

print('     Welcome to Tic Tac Toe')
print('      X always goes first')
print('Enter 0 anytime to quit the game')
print('\n')
display_board()

while current_turn <= MAX_TURNS:
	current_turn += 1
	if current_turn%2 == 0:
		print("O's turn")
		letter = 'O'
	else:
		print("x's turn")
		letter = 'X'
	
	move = int(input('Enter an open position on the board:'))
	
	if move == 0:
		print('User terminated game')
		break

	while move not in range(1,10):
		print('\nInvalid board number selection, try again')
		move = int(input('Enter an open position on the board:'))

	is_open = check_open(move)

	while not is_open:
		print(f'\nThere is already an {board_list[move-1]} in that position, try again')
		move = int(input('Enter an open position on the board:'))
		is_open = check_open(move)

	make_move(letter, move)
	display_board()

	win = check_win()

	if win:
		print(f'{letter} Won! Congrats!')
	elif current_turn == 9:
		print('Draw, no more moves left')

	if current_turn == 9 or win:
		anotha = input('Play again? (y/n): ')
		while anotha != 'y' and anotha != 'n' and anotha != 'Y' and anotha != 'N':
			print('Invalid entry')
			print(anotha)
			anotha = input('Play again? (y/n): ')
		if anotha == 'y' or anotha == 'Y':
			current_turn = 0
			board_list = [1,2,3,4,5,6,7,8,9]
			display_board()
			continue
		else:
			print('Thanks for playing')
			break

	